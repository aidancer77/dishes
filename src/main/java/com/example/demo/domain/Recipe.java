package com.example.demo.domain;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "recipes")
public class Recipe {
    @Id
    //@Column(name = "recipes_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long Id;

    @Column(name = "ingredient_quantity")
    private Long ingredientQuantity;

    @OneToMany
    @JoinTable( name="ingredients",
            joinColumns = @JoinColumn( name="ingredients_id"),
            inverseJoinColumns = @JoinColumn( name="id")
    )
    private List<Ingredient> ingredients;

    @OneToOne
    @JoinTable( name="dishes",
            joinColumns = @JoinColumn( name="dishes_id"),
            inverseJoinColumns = @JoinColumn( name="id")
    )
    private Dish dishes;

    public Recipe(Long id, Long ingredientQuantity, List<Ingredient> ingredients, Dish dishes) {
        Id = id;
        this.ingredientQuantity = ingredientQuantity;
        this.ingredients = ingredients;
        this.dishes = dishes;
    }

    public Recipe() { }

    public Long getId() { return Id; }
    public void setId(Long id) { Id = id; }

    public Long getIngredientQuantity() { return ingredientQuantity; }
    public void setIngredientQuantity(Long ingredientQuantity) { this.ingredientQuantity = ingredientQuantity; }

    public List<Ingredient> getIngredients() { return ingredients; }
    public void setIngredients(List<Ingredient> ingredients) { this.ingredients = ingredients; }

    public Dish getDishes() { return dishes; }
    public void setDishes(Dish dishes) { this.dishes = dishes; }
}
